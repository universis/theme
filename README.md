## @universis/theme
[Universis](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

**@universis/theme** package contains the core theme for building client applications or components for Universis project

![Universis Theme Colors](assets/examples/screenshot1.png)

![Universis Theme Cards](assets/examples/screenshot3.png)

## Usage
    npm i @universis/theme

## Development

Import `@universis/theme` as submodule in any angular cli project by replacing `newProjectRoot` as already configured in your `angular.json`

    git submodule add https://gitlab.com/universis/theme.git <newProjectRoot>/theme

Add the following entry to `tsconfig.app.json#compilerOptions.paths`:

    {
        "compilerOptions": {
            "paths": {
                "@universis/theme/*": [
                    "<newProjectRoot>/theme/*"
                ]
                ...
            }
        }
    }

Import `@universis/theme/scss/universis.scss` in your `style.scss`

    @import "~@universis/theme/scss/universis.scss";

or modify your `angular.json` to use either `@universis/theme/scss/universis.scss` or `@universis/theme/scss/universis.css`

    "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            ...
            "styles": [
              ...
              "@universis/theme/scss/universis.scss",
              ...
            ]
          }
          ...
        }
        ...
    }



